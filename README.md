This playbook provides a shortcut to setting up configuration files for the druw repository application running on docker containers.

The playbook should download either the main druw application https://bitbucket.org/uwlib/druw or your fork of this application.

It assumes docker-sufia images and containers in https://bitbucket.org/uwlib/docker-sufia are already built and running (except for the rails container).

It will then add all the necessary config files to run the druw rails application using those docker-sufia docker containers.

After this builds, you will still have to start the rails application using the correct docker run command as specified in the docker-sufia README. 

`sudo docker run --privileged=true -d -p 127.0.0.1:3300:3000 -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --entrypoint /bin/bash druw:latest -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && /usr/local/bin/rails server"`

Note -  browse_everything_providers.yml is commented out for now.

---
Copy and edit vars.yml.template before running

 - `cp vars.yml.template vars.yml`
 - change 'application_home' value to whatever path you want to clone the druw application into.
 - e.g. /home/youruser/projects/druw
 - change 'application_repo' value to your repo
 - e.g. git@bitbucket.org:bitbucketusername/druw.git

Then run the playbook

`ansible-playbook -K -i inventory playbook.yml`

-K will ask for the machine sudo password. -i will pass the inventory file to use.

Reference

http://kreusch.com.br/blog/2013/12/03/manage-a-development-machine-with-ansible/